This project is a PoC on how to use the message bus in inter-components communications.


It displays a html page with a randomly generated structure of markets/events/selections that communicate between themselves exclusively over the message bus.


The page also shows a floating form at the top left of the page that can be used to manually trigger events directly to the selection buttons components.
Use the input form field to enter the id of the selection that will be targeted and press one of the available event buttons.
It is also possible to target a range of selections by entering the selection ids in the format [min-id]-[max-id].
ex. entering 123-340 and then pressing the 'Disable' button will send a 'disable' event to all buttons with ids between 123 and 340 inclusive.


### install

1. clone project locally
2. 'yarn install'

### execute

1. 'yarn start'.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app)