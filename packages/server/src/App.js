import React, { useState, memo } from 'react'
import { publish } from '@wh/message-bus'
import {
  MarketsContainer,
  EventsContainer,
  SelectionButtonsContainer,
} from '@sb-poc/components'
import './App.css'

const MAX_MARKETS = [20, 100]
const MAX_EVENTS = [3, 50]
const MAX_SELECTIONS = [1, 4]

let selectionId = 0 //make selection ids unique
let eventId = 0 //make event ids unique

const resetIds = () => {
  selectionId = 0
  eventId = 0
}

const generateSelections = () => {
  const numberOfSelections = parseInt(
    MAX_SELECTIONS[0] + Math.random() * MAX_SELECTIONS[1],
    10,
  )
  const selections = []
  for (let k = 0; k < numberOfSelections; k++) {
    selections.push({
      id: selectionId,
      name: `selection ${selectionId}`,
      type: 'default', //use 'auto' to make each button "click itself" once every second
    })
    selectionId += 1
  }
  return selections
}

const generateEvents = () => {
  const numberOfEvents = parseInt(
    MAX_EVENTS[0] + Math.random() * MAX_EVENTS[1],
    10,
  )
  const events = []
  for (let k = 0; k < numberOfEvents; k++) {
    events.push({
      id: eventId,
      active: true,
      name: `event ${k}`,
      selections: generateSelections(),
    })
    eventId += 1
  }
  return events
}

const generateMarkets = () => {
  const numberOfMarkets = parseInt(
    MAX_MARKETS[0] + Math.random() * MAX_MARKETS[1],
    10,
  )
  const markets = []
  for (let k = 0; k < numberOfMarkets; k++) {
    markets.push({
      id: k,
      active: true,
      name: `market ${k}`,
      events: generateEvents(),
    })
  }
  return markets
}

// uncomment to log all the messages being exchanged
// subscribe('button.selection.updated', (topic, payload) => {
//   console.log('received message', topic, payload)
// })
// subscribe('event.updated', (topic, payload) => {
//   console.log('event.updated', topic, payload)
// })

resetIds()

const markets = generateMarkets()
const standAloneEvents = generateEvents()
const standAloneSelections = generateSelections()

const Data = memo(
  () => {
    return (
      <div className="App">
        <MarketsContainer markets={markets}></MarketsContainer>
        <hr />
        <h3> without a render component </h3>
        <SelectionButtonsContainer selections={standAloneSelections} />
        <hr />
        <h3> using a render component </h3>
        <SelectionButtonsContainer
          selections={standAloneSelections}
          render={SelectionButtonsRender}
        />
        <hr></hr>
        <EventsContainer events={standAloneEvents}></EventsContainer>
        <hr></hr>
      </div>
    )
  },
  () => true,
)

const SelectionButtonsRender = ({ selectionButtons, selections }) => {
  const BorderDecorator = (props) => (
    <span style={{ border: '2px dashed tomato' }}>{props.children}</span>
  )
  const [firstButton, ...otherButtons] = selectionButtons
  return (
    <>
      <div>
        <BorderDecorator> {firstButton} </BorderDecorator>
        {otherButtons.map((button, index) => {
          const selection = selections.filter(
            (selection) => selection.id.toString() === button.key,
          )[0]
          return selection.id % 2 === 0 ? button : null
        })}
      </div>
    </>
  )
}

const App = () => {
  const [eventToPublish, setEventToPublish] = useState('')

  const updateEventToPublish = ({ target }) => setEventToPublish(target.value)

  const publishEventToButtons = (topic, payload) => {
    const [min, max] = eventToPublish.split('-')
    for (let k = min; k <= (max || min); k++) {
      publish(`${topic}.${k}`, payload)
    }
  }

  const clickEvent = () => {
    publishEventToButtons('clickcount.increment')
  }

  const enableEvent = () => {
    publishEventToButtons('button.enabled', true)
  }

  const disableEvent = () => {
    publishEventToButtons('button.enabled', false)
  }

  //uncomment next block to randomly publish 'button disable' messages
  //in a short time interval
  // window.setInterval(() => {
  //   const randomSelectionId = parseInt(Math.random() * selectionId, 10)
  //   publish(`button.enabled.${randomSelectionId}`, false)
  // }, 50)

  return (
    <>
      <div
        style={{
          position: 'fixed',
          backgroundColor: '#282c34',
          margin: '5px',
        }}
      >
        <input value={eventToPublish} onChange={updateEventToPublish}></input>
        <button onClick={clickEvent}>Click</button>
        <button onClick={enableEvent}>Enable</button>
        <button onClick={disableEvent}>Disable</button>
      </div>
      <hr></hr>
      <Data></Data>
    </>
  )
}

export default App
