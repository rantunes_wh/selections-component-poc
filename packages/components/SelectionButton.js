import React, { useState, useEffect } from 'react'
import { publish, subscribe, unsubscribe } from '@wh/message-bus'
import SelectionButtonUI from './SelectionButtonUI'

const SelectionButton = ({ selection }) => {
  const [clickCount, setClickCount] = useState(0)
  const [enabled, setEnabled] = useState(true)
  const [managedSelection, setSelection] = useState(selection)

  useEffect(() => {
    if (!managedSelection) {
      return
    }

    const { id, name } = managedSelection
    subscribe(`selection.update.${id}`, (msg, selection) => {
      setSelection(selection)
    })

    subscribe(`clickcount.increment.${id}`, () => {
      setClickCount((currentClickCount) => currentClickCount + 1)
      publish(`button.selection.updated.${id}`, { id, clicks: clickCount })
    })

    subscribe(`button.enabled.${id}`, (msg, enabled) => {
      setEnabled(enabled)
      publish(`button.selection.updated.${id}`, {
        id,
        enabled,
      })
    })

    if (managedSelection.type === 'auto') {
      setInterval(() => {
        //console.log('emiting auto update event for', managedSelection)
        publish(`clickcount.increment.${id}`)
      }, 1000)
    }

    return () => {
      const { id } = managedSelection
      unsubscribe(`selection.update.${id}`)
      unsubscribe(`clickcount.increment.${id}`)
      unsubscribe(`button.enabled.${id}`)
    }
  }, [managedSelection])

  useEffect(() => {
    if (clickCount) {
      publish(`button.selection.updated.${managedSelection.id}`, {
        clicks: clickCount,
      })
    }
  }, [clickCount])

  const text = `${managedSelection.name} ${
    enabled ? 'clicked' : 'updated'
  } ${clickCount} times`

  return (
    <SelectionButtonUI
      onClick={() => setClickCount(clickCount + 1)}
      text={text}
      disabled={!enabled}
    ></SelectionButtonUI>
  )
}

export default SelectionButton
