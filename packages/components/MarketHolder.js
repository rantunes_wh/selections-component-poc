import React, { useState, useEffect, useRef } from 'react'
import { publish, subscribe } from '@wh/message-bus'
import { EventsContainer } from './EventsContainer'

export const MarketHolder = ({ market }) => {
  const { id, events } = market
  const [disabledEvents, _setDisabledEvents] = useState([])
  const enabled = disabledEvents.length < events.length

  //allow access to state inside listener function
  const disabledEventsRef = useRef(disabledEvents)

  const setDisabledEvents = (eventId, enabled) => {
    const { current: currentDisabledEvents } = disabledEventsRef
    let updatedDisabledEvents
    if (enabled) {
      //event was previously disabled, remove it from disabled events array
      if (currentDisabledEvents.includes(eventId)) {
        updatedDisabledEvents = currentDisabledEvents.filter(
          (id) => id !== eventId,
        )
      }
    } else if (!currentDisabledEvents.includes(eventId)) {
      updatedDisabledEvents = [...currentDisabledEvents, eventId]
    }

    if (updatedDisabledEvents) {
      disabledEventsRef.current = updatedDisabledEvents
      _setDisabledEvents(updatedDisabledEvents)
      if (events.length - updatedDisabledEvents.length === 0) {
        publish(`event.updated.${eventId}`, { enabled: false })
      } else if (events.length - currentDisabledEvents.length === 0) {
        publish(`event.updated.${eventId}`, { enabled: true })
      }
    }
  }

  useEffect(() => {
    if (!market) {
      return
    }

    const { events } = market
    events.map(({ id }) => {
      subscribe(`event.updated.${id}`, (_, payload) => {
        const event = events.find(({ id }) => id === payload.id)
        if (event && Object(payload).hasOwnProperty('enabled')) {
          setDisabledEvents(event.id, payload.enabled)
        }
      })
    })

    return () => {
      events.map(({ id }) => {
        unsubscribe(`event.updated.${id}`)
      })
    }
  }, [market])

  return (
    <li>
      <span style={{ color: enabled ? 'green' : 'red' }}>Market {id}</span>
      <span>
        {` (${events.length - disabledEvents.length} / ${events.length})`}
      </span>
      <EventsContainer events={events}></EventsContainer>
    </li>
  )
}
