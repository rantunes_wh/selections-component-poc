import React from 'react'
import { EventHolder } from './EventHolder'

export const EventsContainer = ({ events }) => (
  <ul className="events">
    {events.map((event) => (
      <EventHolder key={event.id} event={event}></EventHolder>
    ))}
  </ul>
)
