import React from 'react'
import { MarketHolder } from './MarketHolder'

export const MarketsContainer = ({ markets }) => {
  return (
    <ul className="markets">
      {markets.map((market) => (
        <MarketHolder key={market.id} market={market}></MarketHolder>
      ))}
    </ul>
  )
}
