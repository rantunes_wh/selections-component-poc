import React from 'react'
import SelectionButton from './SelectionButton'

//creates an array of `SelectionButton`s components from  a list of selections and passes it down to a render Component
export const SelectionButtonsContainer = ({ selections, render }) => {
  //create each individual selection button
  const selectionButtonsGenerator = (selections) =>
    (selections || []).map((selection) => (
      <SelectionButton
        key={selection.id}
        selection={selection}
        hasRender={!!render}
      ></SelectionButton>
    ))

  //create an array of selection buttons components
  const selectionButtons = selectionButtonsGenerator(selections)

  //pass the buttons array down to the render component
  return (
    <>{render ? render({ selectionButtons, selections }) : selectionButtons}</>
  )
}
