import React, { useState, useEffect, useRef } from 'react'
import { publish, subscribe } from '@wh/message-bus'
import { SelectionButtonsContainer } from './SelectionButtonsContainer'

export const EventHolder = ({ event }) => {
  const { id, selections } = event
  const [disabledSelections, _setDisabledSelections] = useState([])
  const enabled = disabledSelections.length < selections.length

  //allow access to state inside listener function
  const disabledSelectionsRef = useRef(disabledSelections)

  const setDisabledSelections = (selectionId, enabled) => {
    const { current: currentDisabledSelections } = disabledSelectionsRef
    let updatedDisabledSelections
    if (enabled) {
      //selection was previously disabled, remove it from disabled selections array
      if (currentDisabledSelections.includes(selectionId)) {
        updatedDisabledSelections = currentDisabledSelections.filter(
          (id) => id !== selectionId,
        )
      }
    } else if (!currentDisabledSelections.includes(selectionId)) {
      updatedDisabledSelections = [...currentDisabledSelections, selectionId]
    }

    if (updatedDisabledSelections) {
      disabledSelectionsRef.current = updatedDisabledSelections
      _setDisabledSelections(updatedDisabledSelections)
      const { id } = event
      if (selections.length - updatedDisabledSelections.length === 0) {
        publish(`event.updated.${id}`, { id, enabled: false })
      } else if (selections.length - currentDisabledSelections.length === 0) {
        publish(`event.updated.${id}`, { id, enabled: true })
      }
    }
  }

  useEffect(() => {
    if (!event) {
      return
    }

    const { selections } = event
    selections.map(({ id }) => {
      subscribe(`button.selection.updated.${id}`, (_, payload) => {
        const selection = selections.find(({ id }) => id === payload.id)
        if (selection && Object(payload).hasOwnProperty('enabled')) {
          setDisabledSelections(selection.id, payload.enabled)
        }
      })
    })

    return () => {
      selections.map(({ id }) => {
        unsubscribe(`button.selection.updated.${id}`)
      })
    }
  }, [event])

  return (
    <li>
      <span style={{ color: enabled ? 'green' : 'red' }}>Event {id}</span>
      <SelectionButtonsContainer
        selections={selections}
      ></SelectionButtonsContainer>
      <span>
        {' '}
        {selections.length - disabledSelections.length} / {selections.length}
      </span>
    </li>
  )
}
