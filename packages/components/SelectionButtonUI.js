import React from 'react'

const SelectionButtonUI = ({ onClick = () => {}, text, disabled }) => (
  <button onClick={onClick} disabled={disabled} data-type="selection">
    {text}
  </button>
)
export default SelectionButtonUI
